/* eslint-disable react/prop-types */
import { Input } from "antd";

const Searcher = ({ searchTerm, setSearchTerm }) => {
  return (
    <Input.Search
      value={searchTerm}
      onChange={(e) => setSearchTerm(e.target.value)}
      placeholder="Buscar..."
    />
  );
};

export default Searcher;
